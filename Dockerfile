FROM unidata/awips-edex-ingest

MAINTAINER John Graham <jjgraham@ucsd.edu>, Dmitry Mishin <dmishin@sdsc.edu>
LABEL Vendor="UCSD" \
      License=GPLv2 \
      Version=1.0
